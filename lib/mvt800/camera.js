class Camera {
  constructor() {
    
  }
  
  /**
   *  save
   *  Get current chunk of buffer data into an image file.
   *  @param  {string}  gpsid        device id
   *  @param  {string}  filename     filename from device, default: timestamp
   *  @param  {integer} total_parts  number of parts, default: 1
   *  @param  {integer} current_part current part number, default: 1
   *  @param  {string}  buffer       current buffer data, hexadecimal
   *  @return {Promise}
   */
  save(gpsid, filename, total_parts, current_part, buffer) {
      var self = this;

      var dir = self.gallery + gpsid + '/';
      if(!fs.lstatSync(dir).isDirectory()) fs.mkdirSync(dir);

      var savedName = dir + (filename || moment().format('YYMMDDHHmmss') + '.jpg');

      return new Promise((resolve, reject) => {
        if (parseInt(total_parts) == 1) {
          fs.writeFile(savedName, buffer, err => {
            if (err) return reject(err);
            resolve(savedName);
          });
        } else {
          var tmpName = savedName + '.part' + current_part;
          fs.writeFile(tmpName, buffer, (err)=>{
            if (err) return reject(err);
            resolve(tmpName);
          });
        }
      })
      .then(checkName => {
        //only single part, should be saved as a picture
        if (checkName === savedName) return;

        //have multiple parts
        if (current_part >= total_parts-1) return self.join(savedName, total_parts);
      })
      .catch(err => {
        console.error(err);
        self.logger("Error while saving " + savedName);
      });
  }
  
  /**
   *  convert image from hex to binary jpg
   *  @param  {string}    filename
   *  @param  {function}  callback
   */
  fix(filename, callback) {
    if (!filename) return;

    var hexFile = filename.replace('jpg', 'hex');
    fs.renameSync(filename, filename.replace('jpg', 'hex'));

    var rstream = fs.createReadStream(hexFile);
    var wstream = fs.createWriteStream(filename);

    rstream.on('data', (chunk)=>{
      var buffer = chunk.toString();
      var newChunk = new Buffer(buffer, 'hex');
      wstream.write(newChunk);
    });

    rstream.on('end', ()=>{
      wstream.end();
      fs.unlink(hexFile);
      if (typeof callback === 'function') callback(filename);
    });
  }
  
  /**
  *  join
  *  @param  {string} fileName
  *  @param  {number} total of parts
  */
  join(fileName, total, callback) {
    return new Promise((resolve, reject) => {
      var wstream = fs.createWriteStream(fileName);
      wstream.on("finish", err => {
        //should delete temporary files
        for (let i=0; i<total; i++) {
          fs.unlink(fileName + '.part' + i);
        }

        resolve(fileName);
      });

      function joinParts(current) {
        if (current >= total) {
            wstream.end();
            return;
        }

        var cstream = fs.createReadStream(fileName + '.part' + current);
        cstream.pipe(wstream, {end: false});

        cstream.on("error", (err)=>{
            reject(err);
        });

        cstream.on("end", ()=>{
          joinParts(current+1);
        });
      }

      joinParts(0);
    });
  }
}

module.exports = new Camera;