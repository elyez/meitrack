const util = require('../util');

module.exports = (socket, data, callback) => {
  //tx,IMEI,AAA,cmd,latitude,longitude,yymmddHHMMSS,Z,sat,GSM,Speed,Heading,HDOP,Altitude,Mileage,Runtime,MCC|MNC|LAC|CI,I/O,AD1|AD2|AD3|Bat|Accu,RFID/Picture/Geofence/TempNo,CustomData,ProtocolVersion,Fuel%[,Temp1|Temp2|Temp3|...TempN]*checksum
  //$$A161,013777008911099,AAA,35,-6.263770,107.001920,150403013916,A,12,12,0,114,0.8,30,288301,573197,510|10|0074|00EA,0002,0000|0000|0000|0A52|03E2,00000001,,1,0000*D7
  
  var alert = parseInt(data[3]) * (data[7] === 'A' ? 1 : -1);
  var assistedInfo = {};
  
  /**************************************************
   *  Additional Data:
   *      - RFID
   *      - Picture
   *      - Geofence
   *      - Temperature Sensor No
   *      - Assisted info
   **************************************************/
  if (!data[19] || data[19].indexOf('*')>-1) {}
  else if ( Math.abs(alert) === 37) {
    assistedInfo.rfid = data[19];
  } else if ( Math.abs(alert) === 39) {
    assistedInfo.picture = data[19];
  } else if ( Math.abs(alert) === 50 || Math.abs(alert) === 51 ) {
    assistedInfo.tempSensorNo = data[19];
  }

  var additionalData = {
    gpstype: "Meitrack",
    rfid: assistedInfo.rfid || ''
  };
  
  /**************************************************
   *  Additional Data:
   *        CELL BASE INFORMATIONS
   **************************************************/
  var base = data[16].split('|');
  base = {
      mcc: parseInt(base[0]),
      mnc: parseInt(base[1]),
      lac: parseInt("0x"+base[2]),
      id : parseInt("0x"+base[3]),
      gsm: parseInt(data[9])
  }
  
  additionalData.signal = base.gsm;
  additionalData.operator = base.mcc + '.' + (base.mnc < 10 ? '0' + base.mnc : base.mnc);
  
  
  /**************************************************
   *  Additional Data:
   *        Analog Sensors & Runtime
   **************************************************/
  var analog = data[18].split('|');
  additionalData.analog = analog[0].split("");
  additionalData.runtime = parseInt(data[15]);

  /***************************************
   *  I/O Handler
   **************************************/
  var io = parseInt(data[17], 16).toString(2).split('').reverse();

  //completing 16bit io
  for (let i=io.length; i<16; i++) {
      io[i] = '0';
  }

  //first 8bit is output, last 8bit is input
  var ioStr = io.join('');
  
  /**************************************************
   *  Additional Data:
   *        BATTERY and ACCU
   *  Device: MT90/T1/T3/MVT100/MVT600/MVT800/TC68S
   **************************************************/
  var ad = data[18].split('|');
  var voltage = [
    (parseInt("0x"+ad[0]) * 3.3 * 2) / 4096,
    (parseInt("0x"+ad[1]) * 3.3 * 2) / 4096,
    (parseInt("0x"+ad[2]) * 3.3 * 2) / 4096,
    (parseInt("0x"+ad[3]) * 3.3 * 2) / 4096,
    (parseInt("0x"+ad[0]) * 3.3 * 16) / 4096
  ];
  additionalData.batt = parseFloat(voltage[3].toFixed(1));
  additionalData.accu = parseFloat(voltage[4].toFixed(1));
  
  /**************************************************
   *  Additional Data:
   *      FUEL SENSORS
   *  Device: MT90/T1/T3/MVT100/MVT600/MVT800/TC68S
   **************************************************/
  if (data[22]) {
    var fuelHexa = data[22].split("");
    var fuelData = parseFloat( (parseInt("0x"+fuelHexa[0]+fuelHexa[1])) + '.' + (parseInt("0x"+fuelHexa[2]+fuelHexa[3])));
    additionalData.fuel = parseFloat(fuelData.toFixed(1));
  }
  
  /**************************************************
   *  Additional Data:
   *      TERMPERATURE SENSORS
   *  Device: MT90/T1/T3/MVT100/MVT600/MVT800/TC68S
   **************************************************/
  if (data[23] && data[23].indexOf('*') < 0) {
    var temp = [];
    additionalData.temperature = [];
    
    if (data[23].indexOf('|') > -1) temp = data[23].split('|');
    else temp.push(data[23]);
    temp.map((str)=>{
      var hexa = str.split("");
      additionalData.temperature.push(parseFloat( (parseInt("0x"+hexa[0]+hexa[1])) + '.' + (parseInt("0x"+hexa[2]+hexa[3]))));
    });
  }
  
  /**************************************************
   *  Populate All Data
   **************************************************/
  var tzo = (new Date()).getTimezoneOffset() * 60000;
  var gps = {
    id        : socket.name,
    gpstime   : util.toTime(data[6].substr(0,2), data[6].substr(2,2), data[6].substr(4,2), data[6].substr(6,2), data[6].substr(8,2), data[6].substr(10,2)),
    satellite : parseInt(data[8]),
    longitude : parseFloat(data[5]),
    latitude  : parseFloat(data[4]),
    altitude  : parseInt(data[13]),
    speed     : parseInt(data[10]),
    heading   : parseInt(data[11]),
    mileage   : parseInt(data[14]) / 1000,
    event     : alert,
    input     : ioStr.substr(8,8),
    output    : ioStr.substr(0,8),
    location  : '',
    received  : new Date().toISOString(),
    others    : additionalData
  };

  return gps;
}