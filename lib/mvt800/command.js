class Command {
  constructor() {
    this.list = [];
  }
  
  /**
   * command constructor to send to socket
   * @param  {string} cmd
   * @return {buffer}
   */
  make(cmd) {
      if (cmd.indexOf('*')>-1)  cmd = cmd.split('*')[0];
      if (cmd.indexOf('@@')>-1) {
        var tmp = cmd.split(',');
        tmp.shift();
        cmd = tmp.join(',');
      }

      var str = '@@Vxx,' + cmd + '*';
          str = str.replace('Vxx', 'V' + (str.length-1) );
      var buffy = new Buffer(str, 'binary');
      var buffAll = 0;
      for (let i=0, N=buffy.length; i<N; i++) {
        buffAll += parseInt(buffy.slice(i,i+1).toString('hex'), 16);
      }
      var checksum = buffAll.toString(16);

      return ( str + checksum.substr(checksum.length-2) + '\r\n' );
  }
  
  /**
   *  sync command sequences
   *  @param  {object} socket  TCP Client
   *  @param  {string} str     full constructed command string
   */
  sync(socket) {
      const self = this;

      if (self.list.length < 1) {
        if (self.check) clearInterval(self.check);
        return;
      }

      self.check = setInterval(()=>{
        //clear me
        clearInterval(self.check);
        
        //if (socket.state > 0) return;

        //send the command
        const str = self.list[0];
        self.send(socket, str);
        self.list.shift();

        //run sync again
        self.sync(socket);
      },100);
  }
}

module.exports = new Command;