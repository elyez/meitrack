const fs = require('fs')
    , path = require('path')
    , parse = require('./parse')
    , camera = require('./camera')
    , command = require('./command');

module.exports = (socket, data, next) => {
  const buf = data.raw.toString().replace(/\r|\n/g, '').split(',');
  if (!socket.name) socket.name = buf[1];
  
  switch (buf[2].toUpperCase()) {
    case 'AAA':
      // parse data
      try {
        const parseData = parse(socket, buf);
        Object.assign(data, parseData);
        next();
      } catch (e) {
        console.error(e);
        next(e);
      }
      break;
      
    case 'D00':
      //MOTION PICTURE
      //$$A480,353358017784062,D00,Filename,Number of picture data packets,Current picture data packet number,Picture data<*checksum>\r\n
      var header = buf[0] + ',' + buf[1] + ',' + buf[2] + ',' + buf[3] + ',' + buf[4] + ',' + buf[5] + ',';
      var pic = new Buffer(data).slice(header.length, data.length-5);

      camera.save(buf[1], buf[3], parseInt(buf[4]), parseInt(buf[5]), pic.toString('hex'))
          .then((filename)=>{
              camera.fix(filename, (file)=>{
                  //delete file from device
                  var cmd = command.make( buf[1]+',D02,'+buf[3]+'|' );
                  socket.write(cmd + "\r\n");

                  //flag device is ready after 2 seconds
                  //socket.state = 0;
                  next();
              });
          });

      //MVT600 send every 8 chunks
      //must request next 8 parts after 2 seconds
      var currentPacket = parseInt(buf[5]) + 1;
      if ( currentPacket % 8 == 0 && currentPacket < parseInt(buf[4]) ) {
        var cmd = command.make( buf[1] + ',D00,' + buf[3] + ',' + currentPacket );
        setTimeout(()=>{
          socket.write(cmd + "\r\n");
        }, 2500);
      }
      break;
      
    case 'D01':
      // GET Picture
      //$$A480,<IMEI>,D01,<Number of packets/page>,<Current part no>,filename1.jpg|filename2.jpg|...|filenameN.jpg|<*checksum>\r\n
      var files = buf[5].split('|').slice(0,-1);
      if (files.length < 1) return;

      var cmd;
      
      for (let i=0, N=files.length; i<N; i++) {
        var filename = self.gallery + buf[1] + '/' + files[i];
        if (fs.existsSync(filename)) {
            cmd = command.make( buf[1]+',D02,'+files[i]+'|' );
            socket.write(cmd + "\r\n");
        } else {
            cmd = command.make( buf[1]+',D00,'+files[i]+',0' );
            command.list.push(str);
        }
      }

      //add another command to get next list
      cmd = command.make( buf[1]+',D01,0' );
      command.list.push(cmd);

      //execute command sequences
      //socket.state = 0;
      command.sync(socket, next);
      break;
      
    default:
      break;
  }
};