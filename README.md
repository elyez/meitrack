# Elyez - Meitrack

[![build status](https://gitlab.com/elyez/meitrack/badges/master/build.svg)](https://gitlab.com/elyez/app/commits/master)

Data parsers for Meitrack / Meiligao devices

## Example

`'app.js'`
```
const Elyez = require('@elyez/gateway');
const Meitrack = require('@elyez/meitrack');

const app = Elyez();
app
  .set('parser', Meitrack.MVT800)
  .listen(PORT);
```


## Packages

* MVT800 / MVT600 (Protocol 1.8 2016-08-09)


## TODO

* More tests & code coverage
* More device types


## License

The MIT License (MIT)