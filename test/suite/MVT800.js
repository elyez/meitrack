const net = require('net');
const path = require('path');
const assert = require('assert');
const moment = require('moment');
const Elyez = require('@elyez/gateway');
const Meitrack = require(__root);

const PORT = 8889;

describe('MVT800', () => {
    const app = Elyez();
    app
    .set('parser', Meitrack.MVT800)
    .listen(PORT);
    
    const client = new net.Socket();
    
    it(`should be able to establish connection to server`, done => {
      client.connect(PORT, '127.0.0.1', done);
      client.on('error', done);
    });
    
    it(`should have id and longitude, latitude data`, done => {
      const str = "$$v141,358899058141774,AAA,25,-6.108450,106.777291,tanggal,A,4,4,speed,252,3.9,23,7942866,23758700,510|1|9494|0090,0800,000A|0009||02E7|0115,*7A";
      
      var sekarang = moment().format('YYMMDDHHmmss');
      var buffer = new Buffer(str.replace('tanggal',sekarang).replace('speed',Math.floor(Math.random()*100+3), app.options.encoding));
      client.write( buffer);

      //fake network delay and app data changes
      setTimeout(function(){
          assert.ok(app.socket.name);
          assert.ok(app.data.id);
          assert.equal(app.socket.name, app.data.id);
          assert.ok(app.data.latitude);
          assert.ok(app.data.longitude);
          done();
      }, 20);
    });
    
    it(`should close server & client connection`, done => {
      client.destroy();
      app.close(done);
    });
});